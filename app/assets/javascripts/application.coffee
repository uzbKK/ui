#= require es5-shim.min
#= require es5-sham.min

#= require jquery
#= require jquery_ujs
#= require jquery.mousewheel
#= require jquery-timing.min
#= require jquery.nicescroll.min
#
#= require bootstrap
#= require bootstrap-switch.min
#
#= require moment
#= require bignumber
#= require underscore
#= require cookies.min
#= require flight.min
#= require pusher.min

#= require ./lib/sfx
#= require ./lib/notifier
#= require ./lib/pusher_connection

#= require highstock
#= require_tree ./highcharts/

#= require_tree ./helpers
#= require_tree ./component_mixin
#= require_tree ./component_data
#= require_tree ./component_ui
#= require_tree ./templates

#= #require rails-ujs
#= #require activestorage
#= #require turbolinks

#= require datafeeds/udf/dist/bundle.js
#= require datafeeds/udf/dist/polyfills.js
#= require charting_library/charting_library.min.js
#= require_tree ./tv_chart_widget

#= require_self

$ ->
  window.notifier = new Notifier()

  BigNumber.config(ERRORS: false)

  HeaderUI.attachTo('header')
  HeaderUI.attachTo('.currency_title_penel')
  AccountSummaryUI.attachTo('#account_summary')

  FloatUI.attachTo('.float')
  KeyBindUI.attachTo(document)
  AutoWindowUI.attachTo(window)

  PlaceOrderUI.attachTo('#bid_entry')
  PlaceOrderUI.attachTo('#ask_entry')
  OrderBookUI.attachTo('#order_book')
  OrderBookUI.attachTo('.bid_orders_panel')
  OrderBookUI.attachTo('.ask_orders_panel')
  DepthUI.attachTo('#depths_wrapper')

  MyOrdersUI.attachTo('#my_orders')
  MyOrdersUI.attachTo('.open_orders_panel')

  MarketTickerUI.attachTo('#ticker')
  MarketSwitchUI.attachTo('#market_list_wrapper')
  MarketSwitchUI.attachTo('.markets_wrapper')
  MarketTradesUI.attachTo('#market_trades_wrapper')
  MarketTradesUI.attachTo('.trade_history_panel')

  ChatUI.attachTo('.chat_wrapper')

  MarketData.attachTo(document)
  GlobalData.attachTo(document, {pusher: window.pusher})
  MemberData.attachTo(document, {pusher: window.pusher}) if gon.accounts

  CandlestickUI.attachTo('#candlestick')
  SwitchUI.attachTo('#range_switch, #indicator_switch, #main_indicator_switch, #type_switch')

  $('.panel-body-content').niceScroll
    autohidemode: true
    cursorborder: "none"

  $('.panel_button').on 'click', ->
      $target = $('.' + $(this).attr('data-target'))
      if $target.hasClass('shown')
        $target.css 'height', 0
        $target.removeClass 'shown'
      else
        $target.css 'height', 'auto'
        $target.addClass 'shown'
      return

  $('.mobile_trade_menu_btn').on 'click', ->
      $menu = $('.mobile_trade_menu')
      $body = $('.mobile_trade_body')
      if $menu.hasClass('opened')
          $menu.removeClass 'opened'
          $body.removeClass 'menu_opened'
      else
          $menu.addClass 'opened'
          $body.addClass 'menu_opened'
      return
  $('.mobile_trade_menu li[data-for^="m_t_"]').on 'click', ->
      $('.mobile_trade_menu li').removeClass 'active'
      $('.mobile_trade_body div[class^="m_t_"]').hide()
      $(this).addClass 'active'
      $('.' + $(this).attr('data-for')).show()
      $('.mobile_trade_menu').removeClass 'opened'
      $('.mobile_trade_body').removeClass 'menu_opened'
