var supportedResolutions = ["1", "3", "5", "15", "30", "60", "120", "240", "D"];

var config = {
    supported_resolutions: supportedResolutions
};
var bars;
var datafunc;
var dataPeriod;
var DataFeed = {
	onReady: cb => {
	console.log('=====onReady running')
		setTimeout(() => cb(config), 0)

	},
	searchSymbols: (userInput, exchange, symbolType, onResultReadyCallback) => {
		console.log('====Search Symbols running')
	},
	resolveSymbol: (symbolName, onSymbolResolvedCallback, onResolveErrorCallback) => {
		// expects a symbolInfo object in response
		console.log('======resolveSymbol running')
		// console.log('resolveSymbol:',{symbolName})
		var split_data = symbolName.split(/[:/]/)
		// console.log({split_data})
		var symbol_stub = {
			name: symbolName,
			description: '',
			type: 'crypto',
			session: '24x7',
			timezone: 'Europe/Athens',
			ticker: symbolName,
			exchange: split_data[0],
			minmov: 1,
			pricescale: 100000000,
			has_intraday: true,
			intraday_multipliers: ['1', '60'],
			supported_resolution:  supportedResolutions,
			volume_precision: 8,
			data_status: 'streaming',
		}

		if (split_data[2].match(/USD|EUR|JPY|AUD|GBP|KRW|CNY/)) {
			symbol_stub.pricescale = 100
		}
		setTimeout(function() {
			onSymbolResolvedCallback(symbol_stub)
			console.log('Resolving that symbol....', symbol_stub)
		}, 0)


		// onResolveErrorCallback('Not feeling it today')

	},
	getBars: function(symbolInfo, resolution, from, to, onHistoryCallback, onErrorCallback, firstDataRequest) {
		console.log('=====getBars running')

        var split_symbol = symbolInfo.name.split(/[:/]/)
        var url = resolution === 'D' ? '/data/histoday' : resolution >= 60 ? '/data/histohour' : '/data/histominute'
        // $.get("https://min-api.cryptocompare.com"+url+'?fsym='+split_symbol[1]+'&tsym='+split_symbol[2]+'&limit=2000', function(data){
        //     var bars = data.Data.map(el => {
        //         return {
        //             time: el.time * 1000, //TradingView requires bar time in ms
        //             low: el.low,
        //             high: el.high,
        //             open: el.open,
        //             close: el.close,
        //             volume: el.volumefrom
        //         }
        //     });
        //     if (bars.length) {
		// 		onHistoryCallback(bars, {noData: false})
		// 	} else {
		// 		onHistoryCallback(bars, {noData: true})
		// 	}
        // })
        // .fail(function(err) {
        //     console.log({err})
		// 	onErrorCallback(err)
        // });
        var tid;
        if(gon.trades.length > 0){
            tid = gon.trades[0].tid;
        }else{
            tid = 0;
        }
        $.get("/api/v2/k_with_pending_trades.json?market="+gon.market.id+"&trade_id="+tid+"&limit=2000&period="+resolution, function(data){
            bars = data.k.map(el => {
                //time open high low close volum
                return {
                    time: el[0] * 1000, //TradingView requires bar time in ms
                    low: el[3],
                    high: el[2],
                    open: el[1],
                    close: el[4],
                    volume: el[5]
                }
            });
            if (bars.length) {
				onHistoryCallback(bars, {noData: false})
			} else {
				onHistoryCallback(bars, {noData: true})
			}
        })
        .fail(function(err) {
            console.log({err})
			onErrorCallback(err)
        });

	},
	subscribeBars: (symbolInfo, resolution, onRealtimeCallback, subscribeUID, onResetCacheNeededCallback) => {
		console.log('=====subscribeBars runnning')
        // var chartChannel = window.pusher.subscribe(createChannelString(symbolInfo));
        // chartChannel.bind('trades', function(data){
        //     console.log('CHART CHANNEL GO');
        // });
        datafunc = onRealtimeCallback;
        dataPeriod = resolution;
        if(typeof last_bar !== "undefined"){
            // var today = new Date();
            // var date = new Date(today.getFullYear(), today.getMonth(), today.getDate(), today.getHour(), 0, 0);
            // last_bar.time = date.getTime();
            onRealtimeCallback(last_bar);
        }
	},
	unsubscribeBars: subscriberUID => {
		console.log('=====unsubscribeBars running')
	},
	calculateHistoryDepth: (resolution, resolutionBack, intervalBack) => {
		//optional
		console.log('=====calculateHistoryDepth running')
		// while optional, this makes sure we request 24 hours of minute data at a time
		// CryptoCompare's minute data endpoint will throw an error if we request data beyond 7 days in the past, and return no data
		return resolution < 60 ? {resolutionBack: 'D', intervalBack: '1'} : undefined
	},
	getMarks: (symbolInfo, startDate, endDate, onDataCallback, resolution) => {
		//optional
		console.log('=====getMarks running')
	},
	getTimeScaleMarks: (symbolInfo, startDate, endDate, onDataCallback, resolution) => {
		//optional
		console.log('=====getTimeScaleMarks running')
	},
	getServerTime: cb => {
		console.log('=====getServerTime running')
	}
};
var last_bar;
$(document).on('market::candlestick::trades',function(e,data){
    // console.log('============IT IS PUSHER DATA');
    // console.log(data);
    // console.log('============END PUSHER');
    // last_bar = {
    //     time: data.candlestick[0][0],
    //     low: data.candlestick[0][3],
    //     high: data.candlestick[0][2],
    //     open: data.candlestick[0][1],
    //     close: data.candlestick[0][4],
    //     volume: data.volume[0].y
    // };
});

function createChannelString(symbolInfo) {
    var channel = symbolInfo.name.split(/[:/]/)
    const exchange = channel[0] === 'GDAX' ? 'Coinbase' : channel[0]
    const to = channel[2]
    const from = channel[1]
    // subscribe to the CryptoCompare trade channel for the pair and exchange
    return `0~${exchange}~${from}~${to}`
}
var my_channel = window.pusher.subscribe("market-"+gon.market.id+"-global");
my_channel.bind('trades', function(data) {
    console.log('An event was triggered with message: ');
    console.log(data.trades[0]);
    var p = parseFloat(data.trades[0].price);
    var v = parseFloat(data.trades[0].amount);
    var today = new Date();
    console.log(dataPeriod);
    if(dataPeriod == 60){
        var date = new Date(today.getFullYear(), today.getMonth(), today.getDate(), today.getHours(), 0, 0);
    }else{
        var date = new Date(today.getFullYear(), today.getMonth(), today.getDate(), today.getHours(), today.getMinutes(), 0);
    }
    console.log(date);
    var x = date.getTime();
    if(typeof last_bar == "undefined"){
        last_bar = bars[bars.length - 1];
    }
    var h = last_bar.high;
    var l = last_bar.low;
    var o = last_bar.open;
    var v = last_bar.volume + v;
    if(p > last_bar.high){
        h = p;
    }else if(p < last_bar.low){
        l = p;
    }
    last_bar = {
        time: x,
        low: l,
        high: h,
        open: o,
        close: p,
        volume: v
    };
    datafunc(last_bar);
    console.log(last_bar);
});
