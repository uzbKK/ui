window.ChatUI = flight.component ->
    objDiv = $('.chat_body')[0]
    $.get '/chat', (response) ->
        $('.chat_body').html response
        objDiv.scrollTop = objDiv.scrollHeight
        return

    setInterval (->
        $.get '/chat', (response) ->
            $('.chat_body').html response
            objDiv.scrollTop = objDiv.scrollHeight
            return
    ), 3000

    @after 'initialize', ->
        objDiv = $('.chat_body')[0]
        $('#send_chat_message').on 'click', ->
            $text = $(this).parent().find('[name="message"]')
            message = $text.val()
            $.post '/chat', {
                    chat:{
                        message: message
                        member_id: 0
                    }
                }, (response) ->
                    $text.val ''
                    $('.chat_body').html response
                    objDiv.scrollTop = objDiv.scrollHeight
                    return
